var React = require('react');
var Catalog = require('./catalog/app-catalog');
var CatalogAll = require('./catalog/app-catalog-all');
var Cart = require('./cart/app-cart');
var Route = require('react-router-component');
var CatalogDetail = require('./product/app-catalogdetail');
var Template = require('./app-template');

var Locations = Route.Locations;
var Location = Route.Location;

var App = React.createClass({
	render: function() {
		return (
			<Template>
				<Locations hash>
                    <Location path="/" handler={CatalogAll} />
                    <Location path="/alphabet" handler={Catalog} />
                    <Location path="/themes" handler={Catalog} />
                    <Location path="/electronic_pubs" handler={Catalog} />
					<Location path="/cart" handler={Cart} />
					<Location path="/item/:section/:item" handler={CatalogDetail} />
				</Locations>
			</Template>
		);
	}
});

module.exports = App;