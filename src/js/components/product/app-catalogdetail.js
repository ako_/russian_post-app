var React = require('react');
var AppStore = require('../../stores/app-store');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin');
var AddToCart = require('../catalog/app-addtocart');
var Link = require('react-router-component').Link;

function getCatalogItem(component) {
    var thisItem;
    AppStore.getCatalog(component.props.section).forEach(function (item) {
        if (item.id.toString() === component.props.item) {
            thisItem = item;
            thisItem.info = AppStore.getCatalogInfo();
        }
    });
    return {item: thisItem};
}

var CatalogDetail = React.createClass({
    mixins: [StoreWatchMixin(getCatalogItem)],
    render: function () {
        return (
                <div>
                <Link href={'/'} className="icon-close js-modal__close"></Link>
                <div className="b-modal-overlay js-modal-overlay open">
                    <Link href={'/'} className="b-modal__close_wrapper"></Link>
                    <div className="b-modal b-modal-effect-1">
                        <section className="b-modal-content b-subscription">

                            <div className="row">
                                <div className="small-12 columns">
                                    <div className="text-center"><img src={this.state.item.img}
                                                                      className="b-subscription__photo"/></div>
                                </div>
                            </div>
                            <div className="b-subscription__type text-center"><i className="icon-book"></i>Журнал
                            </div>
                            <div className="row">
                                <div className="small-10 small-centered columns">
                                    <h1>{this.state.item.title}</h1>

                                    <p className="b-subscription__description">{this.state.item.description}</p>

                                    <p className="b-subscription__info">
                                        <span dangerouslySetInnerHTML={{__html: this.state.item.info.additional}}></span>
                                    </p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="small-3 small-offset-1 columns">
                                    <div className="b-subscription__min">Минимальная<br/>подписка<br/>2 месяца, 1
                                        выпуск
                                    </div>

                                    <div className="b-subscription__price">{this.state.item.info.price1} &#8381;</div>
                                </div>

                                <div className="small-3 columns">
                                    <div className="b-subscription__half-year">Полгода,<br/>2 выпуска
                                        <div className="discount">скидка 30%</div>
                                    </div>

                                    <div className="b-subscription__price">250,39 &#8381;</div>
                                </div>
                                <div className="small-3 columns end">
                                    <div className="b-subscription__year">1 год,<br/>4 выпуска<br/>

                                        <div className="discount">скидка 40%</div>
                                    </div>

                                    <div className="b-subscription__price">1250 &#8381;</div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="small-11 small-centered columns">
                                    <div className="b-card">
                                        <form>
                                            <div className="row">
                                                <div className="small-8 columns">
                                                    <div className="b-card__form-elements">
                                                        <label for="name">Период
                                                            подписки</label>

                                                        <div className="custom-select">
                                                            <select className="chosen">
                                                                <option value=""></option>
                                                                <option value="">Пол года
                                                                </option>
                                                                <option value="">Год
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <label for="name">Тип
                                                            доставки</label>

                                                        <div className="custom-select">
                                                            <select className="chosen">
                                                                <option value="">До
                                                                    адресата
                                                                </option>
                                                                <option value="">Самовывоз
                                                                </option>
                                                            </select>
                                                        </div>
                                                        <label for="address">Адрес</label>
                                                        <div className="custom-address">
                                                            <input id="address" type="text"
                                                                   placeholder="Москва, Маросейка 3/16"/>

                                                            <div className="hint">
                                                                населённый пункт, улица,
                                                                дом, квартира
                                                            </div>
                                                        </div>

                                                        <div>
                                                            <input id="ultype" type="checkbox" name="ultype" />
                                                            <label htmlFor="ultype">Юридическое лицо</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="small-4 columns">
                                                    <div className="b-card__half-year">
                                                        <div className="b-card__price">
                                                            882,18 &#8381;</div>
                                                        Полугодовая подписка
                                                    </div>
                                                    <div className="b-card__delivery">
                                                        <div className="b-card__price">
                                                            120 &#8381;</div>
                                                        Доставка по Москве
                                                    </div>
                                                    <AddToCart item={this.state.item}/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <section className="b-buy-with">
                                <div className="row">
                                    <div className="small-11 small-centered columns">
                                        <h1>Покупают вместе</h1>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="small-11 small-centered columns">
                                        <div className="row">
                                            <div className="small-3 columns">
                                                <div className="title"><a href="#">Американский
                                                    деревянный дом</a></div>
                                                <img src="images//photo-magazine6.jpg"/>
                                            </div>
                                            <div className="small-3 columns">
                                                <div className="title"><a href="#">Американский
                                                    деревянный дом</a></div>
                                                <img src="images//photo-magazine6.jpg"/>
                                            </div>
                                            <div className="small-3 columns">
                                                <div className="title"><a href="#">Американский
                                                    деревянный дом</a></div>
                                                <img src="images//photo-magazine6.jpg"/>
                                            </div>
                                            <div className="small-3 columns">
                                                <div className="title"><a href="#">Американский
                                                    деревянный дом</a></div>
                                                <img src="images//photo-magazine6.jpg"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </section>
                    </div>
                </div>
                </div>
        );
    }
});

module.exports = CatalogDetail;