var React = require('react');

var CatalogSet = React.createClass({
    render: function() {
        return (
                <div className="small-12 columns">
                    <section className="b-selection">
                        <div className="row">
                            <div className="small-7 columns"><img src="images//selection-book1.jpg" /><img
                src="images//selection-book2.jpg" /><img src="images//selection-book3.jpg" /></div>
                            <div className="small-5 columns">
                                <p className="b-selection__choice">подборка</p>
                                <h3 className="b-selection__heading">Авторитетные российские журналы по&nbsp;налогообложению и&nbsp;бухгалтерскому учету</h3>
                                <p className="b-selection__tagline">Все, что нужно бухгалтеру в&nbsp;практической работе. Примеры, образцы и&nbsp;готовые решения, как поступать в&nbsp;неоднозначных ситуациях.</p>
                                <a className="btn btn__primary btn--cart"><i className="icon-cart-plus"></i>В корзину</a>
                            </div>
                        </div>
                    </section>
                </div>
        );
    }
});

module.exports = CatalogSet;