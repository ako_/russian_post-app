var React = require('react');
var AppStore = require('../../stores/app-store');

var StoreWatchMixin = require('../../mixins/StoreWatchMixin');

var CatalogItem = require('./app-catalogitem');
var CatalogItemShort = require('./app-catalogitem-short');
var SimpleCard = require('./app-simplecard');
var Recomended = require('../content/app-recomended');
var CatalogSet = require('./app-catalogset');

function getCatalog() {
    var o = {items: AppStore.getCatalog('journals')};
    return o;
}
function getCatalogByType(type) {
    var o = {items: AppStore.getCatalog(type)};
    return o;
}


var CatalogAll = React.createClass({
    mixins: [StoreWatchMixin(getCatalog)],
    render: function () {
        var el_items = getCatalogByType('electronics');
        var same_items = getCatalogByType('same');
        var press_items = getCatalogByType('press');
        var papers_items = getCatalogByType('papers');
        var lastview_items = getCatalogByType('lastview');
        var items = this.state.items.map(function (item) {
            item.section = 'journals';
            return <CatalogItem key={item.id} item={item}/>
        });
        var electrics = el_items.items.map(function (item) {
            item.section = 'electronics';
            return <CatalogItemShort key={item.id} item={item}/>
        });
        var same = same_items.items.map(function (item) {
            item.section = 'same';
            item.type = 'book';
            return <CatalogItemShort key={item.id} item={item}/>
        });
        var press = press_items.items.map(function (item) {
            item.section = 'press';
            item.type = 'book';
            return <CatalogItemShort key={item.id} item={item}/>
        });
        var papers = papers_items.items.map(function (item) {
            item.section = 'papers';
            item.type = 'book';
            return <CatalogItemShort key={item.id} item={item}/>
        });
        var lastview = lastview_items.items.map(function (item) {
            item.section = 'lastview';
            return <SimpleCard key={item.id} item={item}/>
        });
        return (
                <div>
                    <section className="b-new-popular-magazines">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Новые и популярные журналы</h2>
                            </div>
                        </div>
                        <div className="row">
                            <div className="small-9 columns">
                                <div className="row">
                                    {items}
                                </div>
                            </div>
                            <div className="small-3 columns">
                                <Recomended />
                            </div>
                        </div>
                    </section>
                    <div className="row">
                        <CatalogSet />
                    </div>
                    <section className="b-electronic-publishing">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Электронные издания</h2>
                            </div>
                        </div>
                        <div className="row">
                            {electrics}
                        </div>
                    </section>
                    <section className="b-seems">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Похоже на то, что вы выписываете</h2>
                            </div>
                        </div>
                        <div className="row">
                            {same}
                        </div>
                    </section>
                    <section className="b-press-city">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Пресса Самары</h2>
                            </div>
                        </div>
                        <div className="row">
                            {press}
                        </div>
                    </section>
                    <section className="b-newspaper">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Газеты</h2>
                            </div>
                        </div>
                        <div className="row">
                            {papers}
                        </div>
                    </section>
                    <section className="b-recently-viewed">
                        <div className="row">
                            <div className="small-12 columns">
                                <h2>Вы недавно смотрели</h2>
                            </div>
                        </div>
                        <div className="row">
                            {lastview}
                        </div>
                    </section>
                </div>
        )
    }
});

module.exports = CatalogAll;