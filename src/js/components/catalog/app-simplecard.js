var React = require('react');
var Link = require('react-router-component').Link;


var SimpleCard = React.createClass({
    render: function() {
        return (
                <div className="small-2 columns end">
                    <div className="text-center">
                        <figure><img src={this.props.item.img} />
                            <figcaption>
                                <Link href={'/item/' + this.props.item.section + '/'+this.props.item.id}>{this.props.item.title}</Link>
                            </figcaption>
                        </figure>
                    </div>
                </div>
        );
    }
});

module.exports = SimpleCard;