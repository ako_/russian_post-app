var React = require('react');
var Link = require('react-router-component').Link;
var AddToCart = require('./app-addtocart');


var CatalogItem = React.createClass({
    render: function() {
        return (
                <div className={this.props.item.long?"small-6 columns end":"small-3 columns end"}>
                    <div className={this.props.item.tablet?"b-block-magazine elVersion":"b-block-magazine"}>
                        <Link href={'/item/' + this.props.item.section + '/'+this.props.item.id}>
                        <figure className={this.props.item.subtitle?"text-center":""}>
                            {this.props.item.subtitle ? <div className="together">{this.props.item.subtitle}</div> : ''}
                            <div className={this.props.item.tablet?"":"text-center"}><img src={this.props.item.img} className="magazine"/>{this.props.item.tablet?<img src="images/tablet1-big.jpg" className="tablet"/>:""}</div>
                            <figcaption>
                                {this.props.item.title}
                            </figcaption>
                            <div className="b-description" dangerouslySetInnerHTML={{__html: this.props.item.summary}}></div>
                            <div className="b-price">
                                {this.props.item.cost?<span className={this.props.item.type?"b-price__book":"b-price__mobile"}><i className={this.props.item.type?"icon-book":"icon-mobile"}></i>{this.props.item.old_cost?<span className="strike">{this.props.item.old_cost} &#8381;</span>:"от"} {this.props.item.cost} &#8381;</span>:''}
                                {this.props.item.cost_tablet?<span className="b-price__mobile"><i className="icon-mobile"></i>от {this.props.item.cost_tablet} &#8381;</span>:''}
                            </div>
                        </figure>
                        </Link>
                    </div>
                </div>
            //<AddToCart item={this.props.item} />
        );
    }
});

module.exports = CatalogItem;