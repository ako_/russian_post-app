var React = require('react');

var Header = React.createClass({
	handleClick: function () {
		if (document.getElementById('cabmenu').classList.contains('hidden'))
			document.getElementById('cabmenu').classList.remove('hidden');
		else
			document.getElementById('cabmenu').classList.add('hidden');
		return false;
	},
	render: function() {
		return (
			<div className="row">
				<div className="large-12 columns">
					<header role="banner">
						<div className="row">
							<div className="small-8 columns">
								<a href="/" className="logo"><img src="images/logo.svg" alt="Логотип Почты России" title="Логотип Почты России" /></a>
							</div>
							<div className="small-4 columns">
								<div className="b-account">
									<ul>
										<li className="question"><i className="icon-question"></i><a href="#">Помощь</a></li>
										<li className="menu"><span className="counter">1</span><a href="#" onClick={this.handleClick.bind(this)}>Иван Вотяков</a><i
className="icon-chevron-small-down"></i>
											<div className="b-account__menu hidden" id="cabmenu">
												<ul>
													<li><a href="#">Москва</a></li>
													<li><a href="#">Мои подписки</a></li>
													<li><a href="#">Выйти</a></li>
												</ul>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</header>
				</div>
			</div>
		);
	}
});

module.exports = Header;