var React = require('react');

var Search = React.createClass({
    render: function() {
        return (
                <section className="b-search text-center">
                        <h1>Газеты и журналы по подписке</h1>
                        <input type="text" placeholder="Поиск по индексу, названию, теме или издателю" />
                        <p>Часто ищут</p>
            
                        <div className="tags">
                            <span>Вокруг света</span><span>детские журналы</span><span>бизнес и&nbsp;маркетинг</span><span>бухгалтерам</span><span>японские сканворды</span><span>рецепты</span>
                        </div>
                </section>
        );
    }
});

module.exports = Search;