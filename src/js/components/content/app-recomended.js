var React = require('react');

var Search = React.createClass({
    render: function() {
        return (
                <div className="b-recommendation">
                    <p className="b-recommendation__recommended b-block-recommendation__recommended--tt">рекомендуем</p>
                    <img src="images//recommendation-user.png" />
                    <p className="b-recommendation__recommended">Советует<br/>генеральный директор телеканала РЕН ТВ</p>
                    <p className="b-recommendation__name">Антон Греков</p>
                    <p className="b-recommendation__tagline">Эти журналы помогли мне сделать карьеру</p>
                    <div className="b-recommendation__show"><i className="icon-circle-right"></i><a href="#">Показать</a></div>
                </div>
        );
    }
});

module.exports = Search;