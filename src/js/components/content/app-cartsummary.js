var React = require('react');
var Link = require('react-router-component').Link;
var AppStore = require('../../stores/app-store');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin');

function cartTotals () {
	return AppStore.getCartTotals();
}

var CartSummary = React.createClass({
	mixins: [StoreWatchMixin(cartTotals)],
	render: function() {
		return (
				<div className="b-cart_wrap">
			<section className="b-cart">
				<div className="row">
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
					<div className="small-2 columns">
						<div className="title">АвтоМир <i className="icon-close"></i></div>
						<div className="text-center"><img src="images//photo-magazine3.jpg"/></div>
					</div>
				</div>
				<div className="b-cart__submit">
					<p>{this.state.qty} изданий на {this.state.total} &#8381;</p><a className="btn btn__primary js-trigger-modal">Оформить</a>
				</div>
			</section>
					</div>
		);
	}
});

module.exports = CartSummary;