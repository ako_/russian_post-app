var React = require('react');
var Link = require('react-router-component').Link;


var MenuItem = React.createClass({
    getInitialState: function() {
        return {
            activeMenuId: 'home'
        };
    },
    //setActiveMenuItem: function(uid) {
    //    this.setState({activeMenuId: this.props.item.menuid});
    //},
    render: function() {
        return (
            <span><li><Link className={this.props.item.menuid==this.state.activeMenuId?"active":""} href={this.props.item.link}>{this.props.item.title}</Link></li> </span>
        );
    }
});

module.exports = MenuItem;