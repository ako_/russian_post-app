var React = require('react');
var AppStore = require('../../stores/app-store');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin');
var MenuItem = require('./app-menuitem');

function getMenuList() {
    return {items: AppStore.getMenuList()}
}

var Nav = React.createClass({
    mixins: [StoreWatchMixin(getMenuList)],
    render: function() {
        var items = this.state.items.map(function(item,k) {
            return <MenuItem item={item} />
        });
        return (
                <nav>
                    <div className="row">
                        <div className="small-8 small-centered columns">
                            <ul>
                                {items}
                            </ul>
                        </div>
                    </div>
                </nav>
        );
    }
});

module.exports = Nav;