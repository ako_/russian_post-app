var React = require('react');
var Header = require('./header/app-header');
var Search = require('./content/app-search');
var Footer = require('./footer/app-footer');
var Nav = require('./content/app-nav');
var CartSummary = require('./content/app-cartsummary');

var Template = React.createClass({
	render: function() {
		return (
			<div className="container">
				<Header />
				<Search />
				<section className="b-content" role="main">
					<Nav />
					{this.props.children}
					<CartSummary />
				</section>
				<Footer />
			</div>
		);
	}
});

module.exports = Template;