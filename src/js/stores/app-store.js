var AppConstants = require('../constants/app-constants');
var AppDispatcher = require('../dispatchers/app-dispatcher');
var assign = require('react/lib/Object.assign');
var EventEmitter = require('events').EventEmitter;

var CHANGE_EVENT = 'change';

var _catalog = [],
    _journals = [];

for (var i = 1; i < 9; i++) {
    _catalog.push({
        'id': 'Widget' + i,
        'title': 'Widget #' + i,
        'img': 'http://placehold.it/200x200?text=Widget' + i,
        'summary': 'This is an awesome widget!',
        'description': 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus, commodi.',
        'cost': i
    });
}

    _journals = [{
        'id': 'journal1',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал с эл. версией<br/>1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 1220,
        'cost_tablet': 1220,
        tablet: true
    },{
        'id': 'journal2',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал 1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 1220
    },{
        'id': 'journal3',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал с эл. версией<br/>1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 1220,
        'cost_tablet': 1220,
        tablet: true
    },{
        'id': 'journal4',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал с эл. версией<br/>1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 1220,
        'cost_tablet': 1220,
        tablet: true
    },{
        'id': 'journal5',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал 1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 123,
        'cost_tablet': 82
    },{
        'id': 'journal6',
        'title': 'Forbes',
        'img': 'images/photo-magazine1.jpg',
        'summary': 'Журнал 1 раз в 6 мес.',
        'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
        'cost': 1220
    }];
var _electronics = [{
    'id': 'el1',
    'title': 'Forbes',
    'img': 'images/tablet1-big.jpg',
    'summary': 'Электронный журнал<br/>1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 110
},{
    'id': 'el2',
    'title': 'Forbes',
    'img': 'images/tablet1-big.jpg',
    'summary': 'Электронный журнал<br/>1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277
},{
    'id': 'el3',
    'title': 'Forbes',
    'img': 'images/tablet1-big.jpg',
    'summary': 'Электронный журнал<br/>1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 123
},{
    'id': 'el4',
    'title': 'Forbes',
    'img': 'images/tablet1-big.jpg',
    'summary': 'Электронный журнал<br/>1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277
}];
var _same = [{
    'id': 'same1',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Электронный 1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 110
},{
    'id': 'same2',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Электронный 1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277
},{
    'id': 'same3',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Электронный журнал<br/>1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 123,
    'cost_tablet': 140,
    tablet: true
},{
    'id': 'same4',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Электронный 1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277
}];

var _press = [{
    'id': 'press1',
    'long': true,
    'subtitle': 'ВМЕСТЕ ДЕШЕВЛЕ',
    'title': 'Про здоровье, Про кухню, Аргументы и факты',
    'img': 'images/photo-magazine2.jpg',
    'summary': 'Подписка на 6 месяцев',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 110,
    'old_cost': 143,
},{
    'id': 'press2',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Ежедневная газета',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277
},{
    'id': 'press3',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Ежедневная газета',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 123
}];

var _papers = [{
    'id': 'paper1',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Ежедневная газета',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 110,
    'cost_tablet': 45,
    tablet: true
},{
    'id': 'paper3',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Журнал 1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 123,
    'cost_tablet': 82
},{
    'id': 'paper2',
    'subtitle': 'издательство',
    'title': 'Аргументы и факты',
    'img': 'images/photo-magazine4.jpg',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.'
},{
    'id': 'paper4',
    'title': 'Forbes',
    'img': 'images/photo-magazine1.jpg',
    'summary': 'Журнал 1 раз в 6 мес.',
    'description': 'Национальный познавательный ежемесячный журнал. Издается с 1861 года. Содержит полный электронный архив всех выпусков до 1970 года.',
    'cost': 277,
    'cost_tablet': 32
}];

var _lastview = [{
    'id': 'lview1',
    'title': 'Автомир',
    'img': 'images/photo-magazine3.jpg'
},{
    'id': 'lview2',
    'title': 'Автомир',
    'img': 'images/photo-magazine3.jpg'
}];

var _cataloginfo = {
    price1: 137.03,
    price2: 250.39,
    price3: 1250,
    itemType: 'Журнал',
    additional: '148–164 страницы, 330–410 г. Выходит 1 раз в полгода<br/>Электронная версия: <a href="http://wood.ru" target="_blank">wood.ru</a>'
};

var _menu = [
    {
        menuid: 'home',
        link: '/',
        title: 'Подборки'
    },
    {
        menuid: 'alphabet',
        link: '/alphabet',
        title: 'По алфавиту'
    },
    {
        menuid: 'themes',
        link: '/themes',
        title: 'По теме'
    },
    {
        menuid: 'electronic_pubs',
        link: '/electronic_pubs',
        title: 'Электронные издания'
    }
];

var _cartItems = [];

function _removeItem(index) {
    _cartItems[index].inCart = false;
    _cartItems.splice(index, 1);
}

function _increaseItem(index) {
    _cartItems[index].qty++;
}

function _decreaseItem(index) {
    if (_cartItems[index].qty > 1) {
        _cartItems[index].qty--;
    }
    else {
        _removeItem(index);
    }
}

function _addItem(item) {
    if (!item.inCart) {
        item['qty'] = 1;
        item['inCart'] = true;
        _cartItems.push(item);
    }
    else {
        _cartItems.forEach(function (cartItem, i) {
            if (cartItem.id === item.id) {
                _increaseItem(i);
            }
        });
    }
}

function _cartTotals() {
    var qty = 0, total = 0;
    _cartItems.forEach(function (cartItem) {
        qty += cartItem.qty;
        total += cartItem.qty * cartItem.cost;
    });
    return {'qty': qty, 'total': total};
}

var AppStore = assign(EventEmitter.prototype, {
    emitChange: function () {
        this.emit(CHANGE_EVENT)
    },

    addChangeListener: function (callback) {
        this.on(CHANGE_EVENT, callback)
    },

    removeChangeListener: function (callback) {
        this.removeListener(CHANGE_EVENT, callback)
    },

    getCart: function () {
        return _cartItems
    },

    getCatalogInfo: function () {
        return _cataloginfo
    },
    getCatalog: function (type) {
        if (type == 'catalog')
            return _catalog
        else if (type == 'journals')
            return _journals
        else if (type == 'electronics')
            return _electronics
        else if (type == 'same')
            return _same
        else if (type == 'press')
            return _press
        else if (type == 'papers')
            return _papers
        else if (type == 'lastview')
            return _lastview
    },

    getMenuList: function () {
        return _menu;
    },

    getCartTotals: function () {
        return _cartTotals()
    },

    dispatcherIndex: AppDispatcher.register(function (payload) {
        var action = payload.action; // this is our action from handleViewAction
        switch (action.actionType) {
            case AppConstants.ADD_ITEM:
                _addItem(payload.action.item);
                break;

            case AppConstants.REMOVE_ITEM:
                _removeItem(payload.action.index);
                break;

            case AppConstants.INCREASE_ITEM:
                _increaseItem(payload.action.index);
                break;

            case AppConstants.DECREASE_ITEM:
                _decreaseItem(payload.action.index);
                break;
        }

        AppStore.emitChange();

        return true;
    })

})

module.exports = AppStore;