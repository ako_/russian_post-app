var gulp = require('gulp');
var browserify = require('browserify');
var reactify = require('reactify');
var jshint = require('gulp-jshint');
var source = require('vinyl-source-stream');
var connect = require('gulp-connect');
var sass = require("gulp-sass"),
    util = require("gulp-util"),
    autoprefixer = require('gulp-autoprefixer'),//https://www.npmjs.org/package/gulp-autoprefixer
    minifycss = require('gulp-minify-css'),//https://www.npmjs.org/package/gulp-minify-css
    rename = require('gulp-rename'),//https://www.npmjs.org/package/gulp-rename
    log = util.log;

gulp.task('connect', function () {
    return connect.server({
        'port': 1337,
        'livereload': true,
        'root': 'dist'
    });
});

gulp.task('browserify', function () {
    browserify('./src/js/main.js')
            .transform('reactify')
            .bundle()
            .pipe(source('main.js'))
            .pipe(gulp.dest('dist/js'))
            .pipe(connect.reload());
});

gulp.task('copy', function () {
    gulp.src('src/index.html')
            .pipe(gulp.dest('dist'));
    gulp.src('src/assets/**/*.*')
            .pipe(gulp.dest('dist/assets'))
            .pipe(connect.reload());
});

gulp.task('default', ['browserify', 'copy', 'connect'], function () {
    return gulp.watch('src/**/*.*', ['browserify', 'copy'])
});